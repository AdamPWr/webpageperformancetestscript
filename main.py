import json
import requests
import pandas as pd
import time
from metrics_fether import *
import io 


STRATEGY = ["DESKTOP", "MOBILE"]

fetcher = MetricsFetcher()
result = fetcher.extract_metrics_from_json()

for key in result:
    print('{} : {}'.format(key,result[key]))
