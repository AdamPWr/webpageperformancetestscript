import urllib
import json

class MetricsFetcher:
    

    def __init__(self,url='https://benchmark.baczek.me',strategy='desktop',api_key='AIzaSyD34TI6ChOksnBF_472a0qnVa3wxTWVZnQ'):
        self.URL = url
        self.API_KEY = api_key
        self.STRATEGY = strategy

    def __fetch_raw_results(self):
        result = urllib.request.urlopen('https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url={}&strategy={}&key={}'\
        .format(self.URL,self.STRATEGY,self.API_KEY)).read().decode('UTF-8')

        return result


    def extract_metrics_from_json(self):

        raw_results = self.__fetch_raw_results()
        json_results = json.loads(raw_results)
        json_results = json_results['lighthouseResult']['audits']['metrics']['details']['items'][0]
        return json_results